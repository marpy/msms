#include "debug.h"
#include "networking.h"
#include "protocol.h"

int main()
{
  INFO("Client running...");

  const NetworkAddressInfo *server_address;
  NetworkInfo *net_info = networking_init_client("127.0.0.1",
                                                 MSMS_PORT,
                                                 &server_address);
  if (!net_info) {
    ERROR("Can't init networking!");
    return 0;
  }

  Message *msg= msms_create_package_registration("myname", "mypass");
  networking_send_message(net_info, server_address, msg);
  free(msg);

//  Message *msg = networking_wait_message(net_info, NULL);
//  if (msms_check_package_registration(msg)) {
//    Успешно
//  }



//  networking_send_message(net_info, server_address, header);

  networking_fini(net_info);
  return 0;
}
