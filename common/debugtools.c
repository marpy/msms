#include "debugtools.h"
#include <stdio.h>
#include <ctype.h>


void debugtools_memprint(const char *ptr, size_t lenght)
{
  if (!ptr || !lenght) return;

  printf("Memory at address 0x%lX. Size = %lu:\n",
         (unsigned long) ptr,
         lenght);

  size_t offset = 0;
  for (size_t i = 0; i < (lenght + 16 - lenght % 16); ++i) {

    if (i % 16 == 0) printf("%04lX ", offset);

    if (i < lenght) {
      printf(" %02hhX", ptr[i]);
    } else {
      printf("   ");
    }

    if (i % 4 == 3) printf(" ");

    if (i % 16 == 15) {
      printf("| ");
      for (size_t j = offset; j < i; ++j) {
        if (j < lenght) {
          printf("%c", isprint(ptr[j]) ? ptr[j] : '.');
        } else {
          printf(" ");
        }
      }
      printf("\n");
      offset += 0x10;
    }
  }
  printf("\n");
}
