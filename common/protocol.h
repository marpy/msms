#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stdint.h>
#include <stdlib.h>


#define MSMS_PORT       23456

enum _MsmsPackageType {
  MsmsTypeReserved0,

  MsmsTypeRegistration,
};

typedef enum _MsmsPackageType MsmsPackageType;

struct _Message;
typedef struct _Message Message;

Message *       msms_check_message(void *buffer, size_t size);
int             msms_get_size(const Message *msg);
MsmsPackageType msms_get_type(const Message *msg);
uint32_t        msms_get_src_uid(const Message *msg);
uint32_t        msms_get_dst_uid(const Message *msg);

Message *msms_create_package_registration(const char *username,
                                          const char *userpass);

char * msms_package_registration_username(Message *m);
char * msms_package_registration_userpass(Message *m);

#endif // PROTOCOL_H
