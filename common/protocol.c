#include "protocol.h"

#include <stdlib.h>
#include <string.h>


// Вспомогательные макросы
#define _MAKE_SIGNATURE8(a) ((uint8_t) a)

#define _MAKE_SIGNATURE16(a, b) \
  (((uint16_t) _MAKE_SIGNATURE8(b) << 8) | (uint16_t) _MAKE_SIGNATURE8(a))

#define _MAKE_SIGNATURE32(a, b, c, d) \
  (((uint32_t) _MAKE_SIGNATURE16(c, d) << 16) \
   | (uint32_t) _MAKE_SIGNATURE16(a, b))


#define MSMS_SIGNATURE  _MAKE_SIGNATURE32('S', 'M', 'S', 'M')
#define MSMS_SERVER_UID 0

#pragma pack(push, 1)

struct _MsmsHeader {
  uint32_t signature;  // MSMS
  uint16_t size;
  uint16_t type;
  uint32_t src_uid;
  uint32_t dst_uid;
};

typedef struct _MsmsHeader MsmsHeader;

#define MSMS_USER_NAME_MAX_LEN       60
#define MSMS_USER_PASS_MAX_LEN       60

struct _MsmsContentRegistration {
  char username[MSMS_USER_NAME_MAX_LEN];
  char userpass[MSMS_USER_PASS_MAX_LEN];
};

typedef struct _MsmsContentRegistration MsmsContentRegistration;

struct _MsmsPackageRegistration {
  MsmsHeader header;
  MsmsContentRegistration content;
};

typedef struct _MsmsPackageRegistration MsmsPackageRegistration;


#pragma pack(pop)

Message *msms_check_message(void *buffer, size_t size)
{
  if (!buffer) return NULL;

  // Проверки для протокола
  if ((size_t) size < sizeof(MsmsHeader)) return NULL;

  MsmsHeader *header = buffer;

  // Проверяем сигнатуру и версию
  if (header->signature != MSMS_SIGNATURE) return NULL;

  // Все хорошо пакет вроде наш, теперь проверим длину пакета в заголовке и
  // фактическую
  if (header->size != size) return NULL;

  return header;
}

int msms_get_size(const Message *msg)
{
  if (!msg) return 0;

  return ((MsmsHeader *) msg)->size;
}

MsmsPackageType msms_get_type(const Message *msg)
{
  if (!msg) return 0;

  return (MsmsPackageType) ((MsmsHeader *) msg)->type;
}

uint32_t msms_get_src_uid(const Message *msg)
{
  if (!msg) return 0;

  return ((MsmsHeader *) msg)->src_uid;
}

uint32_t msms_get_dst_uid(const Message *msg)
{
  if (!msg) return 0;

  return ((MsmsHeader *) msg)->dst_uid;
}

static void _fill_header(MsmsHeader *header,
                         size_t size,
                         MsmsPackageType type,
                         uint32_t src_uid,
                         uint32_t dst_uid)
{
  if (!header) return;

  header->signature = MSMS_SIGNATURE;
  header->size = size;
  header->type = type;
  header->src_uid = src_uid;
  header->dst_uid = dst_uid;
}

Message *msms_create_package_registration(const char *username,
                                          const char *userpass)
{
  if (!username || !userpass) return NULL;

  size_t username_len = strlen(username);
  size_t userpass_len = strlen(userpass);

  if (!((0 < username_len && username_len < MSMS_USER_NAME_MAX_LEN)
        && (0 < userpass_len && userpass_len < MSMS_USER_PASS_MAX_LEN))) {
    return NULL;
  }

  MsmsPackageRegistration *p = malloc(sizeof(*p));

  if (!p) return NULL;

  _fill_header(&p->header,
               sizeof(*p),
               MsmsTypeRegistration,
               0,
               MSMS_SERVER_UID);

  strncpy(p->content.username, username, MSMS_USER_NAME_MAX_LEN);
  strncpy(p->content.userpass, userpass, MSMS_USER_PASS_MAX_LEN);

  return p;
}

char *msms_package_registration_username(Message *m)
{
  MsmsPackageRegistration *r = m;
  return r->content.username;
}

char *msms_package_registration_userpass(Message *m)
{
  MsmsPackageRegistration *r = m;
  return r->content.userpass;
}
