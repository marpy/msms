#include "networking.h"

#include <arpa/inet.h>
#include <errno.h>
#include <memory.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

#include "debug.h"
#include "protocol.h"

struct _NetworkAddressInfo {
  struct sockaddr_in address;
};

struct _NetworkInfo {
  int socket;
  NetworkAddressInfo address;
};

NetworkInfo *networking_init_server(uint16_t port)
{
  NetworkInfo *info = malloc(sizeof(NetworkInfo));

  if (!info) {
    ERROR("Can't init server: allocation failed");
    return NULL;
  }

  memset(info, 0, sizeof(NetworkInfo));

  info->socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  if (info->socket == -1) {
    goto error_return;
  }

  info->address.address.sin_family = AF_INET;
  info->address.address.sin_addr.s_addr = htonl(INADDR_ANY);
  info->address.address.sin_port = htons(port);

  if (bind(info->socket,
           (struct sockaddr *) &info->address.address,
           sizeof(info->address)) < 0) {
    goto error_return;
  }

  return info;

error_return:
  ERROR("Can't init server: %s", strerror(errno));
  networking_fini(info);
  return NULL;
}

NetworkInfo *networking_init_client(const char *server_name,
                                    uint16_t server_port,
                                    const NetworkAddressInfo **server_address)
{
  NetworkInfo *info = malloc(sizeof(NetworkInfo));

  if (!info) {
    ERROR("Can't init cli: allocation failed");
    return NULL;
  }

  memset(info, 0, sizeof(NetworkInfo));

  info->socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  if (info->socket == -1) {
    ERROR("Can't init client: %s", strerror(errno));
    networking_fini(info);
    return NULL;
  }

  info->address.address.sin_family = AF_INET;
  info->address.address.sin_port = htons(server_port);

  if (inet_aton(server_name, &info->address.address.sin_addr) == 0) {
    ERROR("Can't init client: can't convert address");
    networking_fini(info);
    return NULL;
  }

  *server_address = &info->address;

  return info;
}

void networking_fini(NetworkInfo *info)
{
  if (!info) return;

  if (info->socket != -1) close(info->socket);
  free(info);
}

Message *networking_wait_message(const NetworkInfo *info,
                                 NetworkAddressInfo **sender)
{
  const char *error_msg = NULL;
  socklen_t slen = sizeof(struct sockaddr_in);
  ssize_t recv_len = 0;

  // Проверяем корректность входных параметров
  if (!info && !sender) return NULL;

  size_t buffer_size = 0xFFFF + 1;  // Максимальный размер по протоколу udp
  void *buffer = malloc(buffer_size);

  if (!buffer) {
    ERROR("Can't allocate memory");
    return NULL;
  }

  // Выделяем память для хранения адреса пользователя
  *sender = malloc(sizeof(NetworkAddressInfo));

  if (!*sender) {
    ERROR("Can't allocate memory");
    free(buffer);
    return NULL;
  }

  // Заполняем буфер нулями
  recv_len = recvfrom(info->socket,
                      buffer,
                      buffer_size,
                      0,
                      (struct sockaddr *) &(*sender)->address,
                      &slen);

  // Проверки получения данных
  if (recv_len == -1) {
    error_msg = strerror(errno);
    goto error_return;
  }

  if (slen != sizeof(struct sockaddr_in)) {
    error_msg = "incorrect address type";
    goto error_return;
  }

  Message *m = msms_check_message(buffer, recv_len);

  if (!m) {
    error_msg = "invalid package";
    goto error_return;
  }

  return m;

error_return:
  ERROR("Can't recive packet: %s", error_msg);
  free(buffer);
  free(*sender);
  return NULL;
}

int networking_send_message(const NetworkInfo *info,
                            const NetworkAddressInfo *destination,
                            const Message *message)
{
  if (!info || !destination || !message) return -1;

  // Отправляем данные
  return sendto(info->socket,
                message,
                msms_get_size(message),
                0,
                (struct sockaddr *) &destination->address,
                sizeof(destination->address));
}
