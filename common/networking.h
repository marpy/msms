#ifndef NETWORKING_H
#define NETWORKING_H

#include <stdint.h>


struct _NetworkInfo;
typedef struct _NetworkInfo NetworkInfo;

struct _NetworkAddressInfo;
typedef struct _NetworkAddressInfo NetworkAddressInfo;

struct _Message;
typedef struct _Message Message;

NetworkInfo *networking_init_server(uint16_t port);
NetworkInfo *networking_init_client(const char *server_name, uint16_t server_port, const NetworkAddressInfo **server_address);

void networking_fini(NetworkInfo *info);

Message *networking_wait_message(const NetworkInfo *info,
                                 NetworkAddressInfo **sender);

int networking_send_message(const NetworkInfo *info,
                            const NetworkAddressInfo *destination,
                            const Message *message);

#endif // NETWORKING_H
