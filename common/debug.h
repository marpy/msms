#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>

#define PRINT_DEBUG_MESSAGE     1
#define PRINT_INFO_MESSAGE      1
#define PRINT_ERROR_MESSAGE     1

#if PRINT_DEBUG_MESSAGE
#define DEBUG(msg, ...) \
  printf("[DBG %10.10s:% 4d] " msg "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define DEBUG(msg, ...)
#endif

#if PRINT_INFO_MESSAGE
#define INFO(msg, ...) \
  printf("[INF %10.10s:% 4d] " msg "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define INFO(msg, ...)
#endif

#if PRINT_ERROR_MESSAGE
#define ERROR(msg, ...) \
  printf("[ERR %10.10s:% 4d] " msg "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define ERROR(msg, ...)
#endif

#endif // DEBUG_H
