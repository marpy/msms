#include "debug.h"
#include "debugtools.h"
#include "networking.h"
#include "protocol.h"

static void server_process_registration(const Message *m,
                                        const NetworkAddressInfo *client)
{
  INFO("Processing registration");
  const char *username = msms_package_registration_username(m);
  const char *userpass = msms_package_registration_userpass(m);

  DEBUG("Registration: username: %s, userpass: %s",
        username,
        userpass);

  userid = server_register_user(username, userpass);
}

int main()
{
  INFO("Server running...");

  NetworkInfo *net_info = networking_init_server(MSMS_PORT);

  for (;;) {
    NetworkAddressInfo *client;
    Message *msg = networking_wait_message(net_info, &client);

    if (!msg) {
      ERROR("Can't get message!");
      continue;
    }

    switch (msms_get_type(msg)) {
    case MsmsTypeRegistration:
      server_process_registration(msg, client);
      break;

    default:
      ERROR("Unknown package");
      break;
    }
    debugtools_memprint(msg, msms_get_size(msg));
    free(msg);
  }

  networking_fini(net_info);

  return 0;
}
